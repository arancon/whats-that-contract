pragma solidity ^0.4.18;

// This is just a simple example of a coin-like contract.
// It is not standards compatible and cannot be expected to talk to other
// coin/token contracts. If you want to create a standards-compliant
// token, see: https://github.com/ConsenSys/Tokens. Cheers!

contract WhatsThat {
	mapping (address => uint) balances;

	event Transfer(address indexed _from, address indexed _to, uint256 _value);

	constructor() public {
	}

	function addPoints(address receiver, uint points) public returns(bool sufficient) {
		balances[receiver] += points;
		emit Transfer(msg.sender, receiver, points);
		return true;
	}

	function getPoints(address addr) public view returns(uint) {
		return balances[addr];
	}

	function getAddress() public view returns(address) {
		return address(this);
	}
}
