pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/WhatsThat.sol";

contract TestWhatsThat {

  function testInitialPointsUsingDeployedContract() public {
    WhatsThat whatsthat = WhatsThat(DeployedAddresses.WhatsThat());

    uint expected = 0;

    Assert.equal(whatsthat.getPoints(tx.origin), expected, "Owner should have 0 points initially");
  }

  function testInitialPointsWithNewMetaCoin() public {
    WhatsThat whatsthat = new WhatsThat();

    uint expected = 0;

    Assert.equal(whatsthat.getPoints(tx.origin), expected, "Owner should have 0 MetaCoin initially");
  }

}
