var WhatsThat = artifacts.require("./WhatsThat.sol");

contract('WhatsThat', function(accounts) {
  it("should put 0 points in the first account", function() {
    return WhatsThat.deployed().then(function(instance) {
      return instance.getPoints.call(accounts[0]);
    }).then(function(points) {
      assert.equal(points.valueOf(), 0, "0 wasn't in the first account");
    });
  });
	var meta;	
	it("should put 20 points in the second account"+accounts[0], function() {
    return WhatsThat.deployed().then(function(instance) {
			meta = instance;
      return meta.addPoints(accounts[1], 20, {from: accounts[0]});
    }).then(function() {    
			return meta.getPoints.call(accounts[1]);
		}).then(function(points) {
      assert.equal(points.valueOf(), 20, "20 wasn't in the first account");
    });
  });
});

